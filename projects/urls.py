from django.urls import path
from projects.views import show_projects_list, project_detail, create_project

urlpatterns = [
    path("", show_projects_list, name="list_projects"),
    path("<int:id>/", project_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
