from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def show_projects_list(request):
    projects_list = Project.objects.filter(owner=request.user)
    context = {"projects_list": projects_list}
    return render(request, "projects_list/projects_list.html", context)


@login_required
def project_detail(request, id):
    project = project = Project.objects.get(id=id)
    context = {"project": project}
    return render(request, "project_detail/project_detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(
        request, "project_create/project_create.html", {"form": form}
    )
